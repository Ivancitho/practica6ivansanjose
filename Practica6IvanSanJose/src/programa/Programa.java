package programa;

import java.util.Scanner;

import clases.Juego;

public class Programa {

	public static void main(String[] args) {
		
	Scanner input = new Scanner(System.in);
	
	System.out.println("Creamos una Nave estelar");
	Juego naveEstelar = new Juego();
	
	System.out.println("1. Creamos las naves que queramos y las mostramos");
	naveEstelar.altaNave();
	naveEstelar.listarNaves();
	
	System.out.println("");
	System.out.println("2. Buscamos las naves o nave que queramos por su nombre");
	String nombre = input.nextLine();
	System.out.println(naveEstelar.buscarNave(nombre));
	
	System.out.println("");
	System.out.println("3. Creamos los Enemigos del Imperio que queramos");
	naveEstelar.altaEnemigoImperio();
	naveEstelar.listarEnemigoImperio();
	
	System.out.println("");
	System.out.println("4. Buscamos los Enemigos del Imperio que queramos, por su division");
	String division = input.nextLine();
	System.out.println(naveEstelar.buscarEnemigoImperio(division));
	
	System.out.println("");
	System.out.println("5. Creamos los Sith que queramos");
	naveEstelar.altaSith();
	naveEstelar.listarSith();
	
	System.out.println("");
	System.out.println("6. Buscar Sith que queramos por su nombre");
	String nombre1 = input.nextLine();
	System.out.println(naveEstelar.buscarSith(nombre1));
	
	System.out.println("");
	System.out.println("7. Creamos los Droides que queramos");
	naveEstelar.altaDroide();
	naveEstelar.listarDroide();
	
	System.out.println("");
	System.out.println("8. Buscar Droide que queramos por su utilidad");
	String nombre2 = input.nextLine();
	System.out.println(naveEstelar.buscarDroide(nombre2));
	
	System.out.println("");
	System.out.println("9. Eliminamos las naves que queramos por su nombre");
	String nombreNave = input.nextLine();
	naveEstelar.eliminarNave(nombreNave);
	naveEstelar.listarNaves();
	
	System.out.println("");
	System.out.println("10. Eliminamos los Enemigos del Imperio que queramos por su nombre");
	String divisionEnemigoImperio = input.nextLine();
	naveEstelar.eliminarEnemigoImperio(divisionEnemigoImperio);
	naveEstelar.listarEnemigoImperio();
	
	System.out.println("");
	System.out.println("11. Eliminamos los Sith que queramos por su nombre");
	String nombreSith = input.nextLine();
	naveEstelar.eliminarSith(nombreSith);
	naveEstelar.listarSith();
	
	System.out.println("");
	System.out.println("12. Eliminamos los Droides que queramos por su utilidad");
	String nombreDroide = input.nextLine();
	naveEstelar.eliminarSith(nombreDroide);
	naveEstelar.listarDroide();
	
	input.close();
	}

}
