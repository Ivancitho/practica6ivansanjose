package clases;

public class Sith extends EnemigoImperio{

	String nombre;
	boolean fuerza;
	String altura;
	String planeta;
	
	public Sith(String arma, String division, String nombre, boolean fuerza, String altura, String planeta) {
		super(arma, division);
		this.nombre = nombre;
		this.fuerza = fuerza;
		this.altura = altura;
		this.planeta = planeta;
	}
	
	public Sith() {
		super();
		this.nombre = "";
		this.fuerza = true;
		this.altura = "";
		this.planeta = "";
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isFuerza() {
		return fuerza;
	}

	public void setFuerza(boolean fuerza) {
		this.fuerza = fuerza;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getPlaneta() {
		return planeta;
	}

	public void setPlaneta(String planeta) {
		this.planeta = planeta;
	}

	@Override
	public String toString() {
		return "Sith [nombre=" + nombre + ", fuerza=" + fuerza + ", altura=" + altura + ", planeta=" + planeta
				+ ", toString()=" + super.toString() + "]";
	}
	
	public int ingresoTrabajo() {
		creditos+= 10;
		return creditos;
	}
	
	public void llenarSith() {
		System.out.println("Introduce los datos del Enemigo");
		System.out.println("Nombre, fuerza, altura y planeta respectivamente");
		this.nombre = input.next();
		this.fuerza = input.nextBoolean();
		this.altura = input.next();
		this.planeta = input.next();
	}
	
}
