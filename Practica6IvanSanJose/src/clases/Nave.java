package clases;

import java.util.Scanner;

public class Nave {

	Scanner input = new Scanner(System.in);
	
	private String nombre;
	private String modelo;
	private int capacidad;
	
	public Nave(String nombre, String modelo, int capacidad) {
		this.nombre = nombre;
		this.modelo = modelo;
		this.capacidad = capacidad;
	}
	
	public Nave() {
		this.nombre = "";
		this.modelo = "";
		this.capacidad = 0;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	@Override
	public String toString() {
		return "Nave [nombre=" + nombre + ", modelo=" + modelo + ", capacidad=" + capacidad + "]";
	}
	
	public void llenarNave() {
		System.out.println("Introduce los datos de la nave");
		System.out.println("Nombre, modelo y capacidad respectivamente");
		this.nombre = input.next();
		this.modelo = input.next();
		this.capacidad = input.nextInt();
	}
	
}
