package clases;

public class Droide extends EnemigoImperio{

	String tipo;
	String utilidad;
	String tipoProgramacion;
	
	public Droide(String arma, String division,String tipo, String utilidad, String tipoProgramacion) {
		super(arma, division);
		this.tipo = tipo;
		this.utilidad = utilidad;
		this.tipoProgramacion = tipoProgramacion;
	}
	
	public Droide() {
		super();
		this.tipo = "";
		this.utilidad = "";
		this.tipoProgramacion = "";
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUtilidad() {
		return utilidad;
	}

	public void setUtilidad(String utilidad) {
		this.utilidad = utilidad;
	}

	public String getTipoProgramacion() {
		return tipoProgramacion;
	}

	public void setTipoProgramacion(String tipoProgramacion) {
		this.tipoProgramacion = tipoProgramacion;
	}

	@Override
	public String toString() {
		return "Droide [tipo=" + tipo + ", utilidad=" + utilidad + ", tipoProgramacion=" + tipoProgramacion
				+ ", toString()=" + super.toString() + "]";
	}

	public int pagoTrabajo() {
		creditos-= 10;
		return creditos;
	}
	
	public void llenarDroide() {
		System.out.println("Introduce los datos del Enemigo");
		System.out.println("Tipo, utilidad y tipo de programacion respectivamente");
		this.tipo = input.next();
		this.utilidad = input.next();
		this.tipoProgramacion = input.next();
	}
	
}
