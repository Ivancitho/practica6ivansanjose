package clases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Juego {

	Scanner input = new Scanner(System.in);
	
	private ArrayList<Nave> listaNaves;
	private ArrayList<EnemigoImperio> listaEnemigosImperio;
	private ArrayList<Sith> listaSith;
	private ArrayList<Droide> listaDroides;
	
	public Juego() {
		listaNaves = new ArrayList<Nave>();
		listaEnemigosImperio = new ArrayList<EnemigoImperio>();
		listaSith = new ArrayList<Sith>();
		listaDroides = new ArrayList<Droide>();
	}
	
	public void altaNave() {
		String respuesta = "";
		do {
			Nave nuevaNave = new Nave();
			nuevaNave.llenarNave();
			listaNaves.add(nuevaNave);
			System.out.println("�Quiere introducir otra Nave? (si/no)");
			respuesta = input.next();
			
		}while(respuesta.toLowerCase().trim().equalsIgnoreCase("si"));
	}
	
	public void listarNaves() {
		for(Nave nave : listaNaves) {
			if(nave != null) {
				System.out.println(nave);
			}
		}
	}
	
	public Nave buscarNave(String nombre) {
		for(Nave nave : listaNaves) {
			if(nave != null && nave.getNombre().equals(nombre)) {
				return nave;
			}
		}
		return null;
	}
	
	public void altaEnemigoImperio() {
		String respuesta = "";
		do {
			EnemigoImperio nuevoEnemigoImperio = new EnemigoImperio();
			nuevoEnemigoImperio.llenarEnemigoImperio();
			listaEnemigosImperio.add(nuevoEnemigoImperio);
			System.out.println("�Quiere introducir otro Enemigo del Imperio? (si/no)");
			respuesta = input.next();
			
		}while(respuesta.toLowerCase().trim().equalsIgnoreCase("si"));
	}
	
	public void altaSith() {
		String respuesta = "";
		do {
			Sith nuevoSith = new Sith();
			nuevoSith.llenarSith();
			listaSith.add(nuevoSith);
			System.out.println("�Quiere introducir otro Sith? (si/no)");
			respuesta = input.next();
			
		}while(respuesta.toLowerCase().trim().equalsIgnoreCase("si"));
	}
	
	public void altaDroide() {
		String respuesta = "";
		do {
			Droide nuevoDroide = new Droide();
			nuevoDroide.llenarDroide();
			listaDroides.add(nuevoDroide);
			System.out.println("�Quiere introducir otro Droide? (si/no)");
			respuesta = input.next();
			
		}while(respuesta.toLowerCase().trim().equalsIgnoreCase("si"));
	}
	
	public void listarEnemigoImperio() {
		for(EnemigoImperio enemigoImperio : listaEnemigosImperio) {
			if(enemigoImperio != null) {
				System.out.println(enemigoImperio);
			}
		}
	}
	
	public void listarSith() {
		for(Sith sith : listaSith) {
			if(sith != null) {
				System.out.println(sith);
			}
		}
	}
	
	public void listarDroide() {
		for(Droide droide : listaDroides) {
			if(droide != null) {
				System.out.println(droide);
			}
		}
	}
	
	public EnemigoImperio buscarEnemigoImperio(String division) {
		for(EnemigoImperio enemigoImperio : listaEnemigosImperio) {
			if(enemigoImperio != null && enemigoImperio.getDivision().equals(division)) {
				return enemigoImperio;
			}
		}
		return null;
	}
	
	public Sith buscarSith(String nombre) {
		for(Sith sith : listaSith) {
			if(sith != null && sith.getNombre().equals(nombre)) {
				return sith;
			}
		}
		return null;
	}
	
	public Droide buscarDroide(String utilidad) {
		for(Droide droide : listaDroides) {
			if(droide != null && droide.getUtilidad().equals(utilidad)) {
				return droide;
			}
		}
		return null;
	}
	
	public void eliminarEnemigoImperio(String divisionEnemigoImperio) {
		Iterator<EnemigoImperio> itEnemigoImperio = listaEnemigosImperio.iterator();
		while (itEnemigoImperio.hasNext()) {
			EnemigoImperio enemigoImperio = itEnemigoImperio.next();
			if(enemigoImperio.getDivision().equals(divisionEnemigoImperio)) {
				itEnemigoImperio.remove();
			}
		}
	}
	
	public void eliminarNave(String nombreNave) {
		Iterator<Nave> itnave = listaNaves.iterator();
		while (itnave.hasNext()) {
			Nave nave = itnave.next();
			if(nave.getNombre().equals(nombreNave)) {
				itnave.remove();
			}
		}
	}
	
	public void eliminarSith(String nombreSith) {
		Iterator<Sith> itsith = listaSith.iterator();
		while (itsith.hasNext()) {
			Sith sith = itsith.next();
			if(sith.getNombre().equals(nombreSith)) {
				itsith.remove();
			}
		}
	}
	
	public void eliminarDroide(String utilidad) {
		Iterator<Droide> itDroide = listaDroides.iterator();
		while (itDroide.hasNext()) {
			Droide droide = itDroide.next();
			if(droide.getUtilidad().equals(utilidad)) {
				itDroide.remove();
			}
		}
	}
	
}
