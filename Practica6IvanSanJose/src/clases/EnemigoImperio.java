package clases;

import java.util.Scanner;

public class EnemigoImperio {

	Scanner input = new Scanner(System.in);
	
	String tipo;
	String arma;
	String division;
	int creditos;
	Nave pasajero;
	
	public EnemigoImperio(String tipo, String arma, String division) {
		this.tipo = tipo;
		this.arma = arma;
		this.division = division;
	}
	
	public EnemigoImperio(String tipo, String division) {
		this.tipo = tipo;
		this.division = division;
	}
	
	public EnemigoImperio() {
		this.tipo = "";
		this.arma = "";
		this.division = "";
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getArma() {
		return arma;
	}

	public void setArma(String arma) {
		this.arma = arma;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Nave getPasajero() {
		return pasajero;
	}

	public void setPasajero(Nave pasajero) {
		this.pasajero = pasajero;
	}

	@Override
	public String toString() {
		return "EnemigoImperio [tipo=" + tipo + ", arma=" + arma + ", division=" + division + ", pasajero=" + pasajero
				+ "]";
	}
	
	public int ingresoCreditos(int dinero) {
		creditos += dinero;
		return creditos;
	}
	
	public void llenarEnemigoImperio() {
		System.out.println("Introduce los datos del Enemigo");
		System.out.println("Tipo, arma division y creditos respectivamente");
		this.tipo = input.next();
		this.arma = input.next();
		this.division = input.next();
		this.creditos = input.nextInt();
	}
	
}
